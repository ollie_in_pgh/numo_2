# Numo (2) Autocomplete

## Table of Contents

[[_TOC_]]

## How to Use Numo 2 Autocomplete

Open a browser or use `curl`, ending the url with the prefix of a word you wish to check.

### Example Request

```shell script
curl -X GET \
    https://8fjeg6lhu6.execute-api.us-east-2.amazonaws.com/dev/autocomplete/blah
```

### Example Response

```json
[
  "blah",
  "blahs",
  "blahed",
  "blaher",
  "blahest",
  "blahing"
]
```

## How to run Numo 2 Autocomplete Locally

Numo 2 Autocomplete is Dockerized. You must have Docker running on your local laptop.

To build locally, execute the `build.sh` file. You may need to change its permissions first.

In the `numo_2` directory run:

```shell script
chmod +x build.sh
./build.sh
```

After it builds, run the `run.sh` script:

```shell script
chmod +x run.sh
./run.sh
```

Check url `http://127.0.0.1:5000/autocomplete/blah` replacing "blah" with your prefix.

## How to deploy Numo 2 Autocomplete to Lambda
NOTE: REQUIRES PYTHON 3.6, 3.7, or 3.8
Numo 2 Autocomplete uses Zappa to deploy to lambda. To deploy, your AWS user must be allowed to deploy to lambda through
IAM permission. Ask to be added to this group.

After your permissions are clear, and you have the AWS CLI installed run:

```shell script
chmod +x deploy.sh
./deploy.sh
```

The only available build currently is `dev`.

## How to dev in Numo 2 Autocomplete

To add an autocomplete service (elasticsearch or redis, etc.) create a new searcher class in `app/service/handler.py`
then add your new searcher class to the registry in the `Search` class. Call it with a unique key.

## How Numo 2 Autocomplete works

### DAWG Search
The DawgSearcher uses `fast-autocomplete`, a directed acyclic word graph library. Info on `fast-autocomplete` can be
found [here](https://pypi.org/project/fast-autocomplete/) and [here](https://zepworks.com/posts/you-autocomplete-me/).
The `_DawgSearcher` class, which uses `fast-autocomplete`, is instantiated at runtime and cached.

I chose `fast-autocomplete` because it's cached.

### Trie Search
The TrieSearch uses a Prefix Trie. The Trie searches via depth first search and uses a stack rather than recursion. A 
stack was chosen over recursion to manage memory and to avoid `RuntimeError: maximum recursion depth exceeded` exceptions.

## Scaling the Scrabble corpus

The Scrabble corpus is currently 3 MB. This is loaded into `fast-autocomplete` at runtime and requests for prefixes are
cached. If it's determined to use a heavier corpus, Numo 2 Autocomplete should pull the Scrabble corpus file from S3 (
instead of checking it in with git).

If caching coupled with the Scrabble corpus becomes too memory-intensive a new system must be designed, possibly using
Redis or Hadoop or Elasticsearch.

## Scaling requests

AWS Lambda currently auto scales with requests. If Numo 2 Autocomplete is to be used as a standalone service (possibly
within a microservice architecture) and needs to be scaled, Docker should plug into a scaling platform such as
Kubernetes or AWS Batch.

## Time Spent and Unlimited Time

About four hours were used to create the Flask server and everything in the `app` directory.

Deployment to Lambda took a lot of time dedicated to troubleshooting because Lambda won't install python packages that
compile C code, the `Levenshtein` package has to be downloaded and unpacked with a Linux `so` file.

Another couple hours were used to troubleshoot Docker, which can be both awesome and maddening.

If there was unlimited time, I would build out the AWS backend further. I'd prioritize AWS resources and build them out
using Terraform. There are already S3 buckets, but a domain could be registered and used through Route 53. I would also 
implement a memoize class for the TrieSearcher.

## Self Critique

I'm not incredibly happy with the registry pattern in `Search`. It's okay, but I feel a different pattern, using mixins
instead of inheritance could be used. 

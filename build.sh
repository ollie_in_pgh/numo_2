#!/usr/bin/env bash

IMAGE="numo_2"

# prune Docker, it can get a bit jungly
# docker system prune -a -f
docker build  -t ${IMAGE} .
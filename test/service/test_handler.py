import logging
import pathlib

import pytest

from app.service.handler import FileHandler, Search

logger = logging.getLogger(__name__)
msg = "Testing handlers"
logger.info(msg)


@pytest.fixture()
def file_handler():
    test_dir = pathlib.Path(__file__).parents[1]
    data_dir = pathlib.Path.joinpath(test_dir, "mock_data")
    file_name = "words_small.txt"
    words_path = pathlib.Path.joinpath(data_dir, file_name)
    return FileHandler(words_path)


@pytest.fixture()
def scrabble_words(file_handler):
    return file_handler.get_scrabble_corpus()


@pytest.fixture()
def dawg_searcher(scrabble_words):
    searcher = Search(corpus=scrabble_words)
    return searcher


@pytest.fixture()
def trie_searcher(scrabble_words):
    searcher = Search("trie_searcher", corpus=scrabble_words)
    return searcher


def test_file_handler_get_words(scrabble_words):
    assert len(scrabble_words) == 101
    assert "MIDLIST".lower() in scrabble_words
    assert "APPLE" not in scrabble_words
    assert "APPLE".lower() not in scrabble_words


def test_dawg_searcher(dawg_searcher):
    result = dawg_searcher.search("mid")
    assert len(result) == 10
    assert "MIDI".lower() in result


def test_dawg_searcher_fail(dawg_searcher):
    result = dawg_searcher.search("app")
    assert not result


def test_trie_searcher(trie_searcher):
    result = trie_searcher.search("mid")
    assert len(result) == 10


def test_trie_searcher_fail(trie_searcher):
    result = trie_searcher.search("app")
    assert not result

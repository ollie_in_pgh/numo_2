import logging

import pytest

from app import create_app
from app.config import TestConfig

logger = logging.getLogger(__name__)


@pytest.fixture(scope="session")
def app():
    app = create_app(name="test", configuration=TestConfig)
    app.config["TESTING"] = True

    yield app


@pytest.fixture(scope="function")
def client(app):
    logger.debug("Opening test client")
    client = app.test_client()
    yield client

    # any cleanup goes here
    logger.debug("Closing test client")

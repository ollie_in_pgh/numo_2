def test_get_by_prefix(client):
    result = client.get('/autocomplete/blah')
    assert result.status_code == 200
    response = result.get_json()
    assert len(response) == 6
    assert "blahest" in response

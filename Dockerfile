FROM python:3.7-slim-buster
RUN apt-get update -y && apt-get install -y gcc python3-dev
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app
ENTRYPOINT ["python"]
CMD ["numo_2.py"]
import logging
import os
import sys
from logging.handlers import RotatingFileHandler

from flask import Flask
from flask_cors import CORS

from app.config import CONFIG_MAP
from app.service.handler import Search

cors = CORS()
searcher = Search()


def register_blueprints(app):
    from .autocomplete import autocomplete_bp
    app.register_blueprint(autocomplete_bp, url_prefix='/autocomplete')


def setup_logger(app):
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    if app.config["LOG_TO_FILE"]:
        if not os.path.exists("logs"):
            os.mkdir("logs")
        handler = RotatingFileHandler(
            "logs/numo_2.log",
            maxBytes=app.config["LOGMAXBYTES"],
            backupCount=app.config["LOGBACKUPCOUNT"]
        )
        handler.setFormatter(formatter)
        handler.setLevel(logging.INFO)
        app.logger.addHandler(handler)

    if app.config["LOG_TO_STD_OUT"]:
        handler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(formatter)
        handler.setLevel(logging.DEBUG)
        app.logger.addHandler(handler)

    app.logger.info("Numo Startup")


def create_app(name="numo_2", configuration=None):
    """ Create app using factory pattern
    :param name:
    :param configuration:
    :return: Flask app
    """
    app = Flask(name)

    if configuration is None:
        mode = os.environ.get("ENV", "development")
        configuration = CONFIG_MAP[mode]
        config.ENV = mode

    app.config.from_object(configuration())
    app.debug = app.config['DEBUG']
    cors.init_app(app)
    register_blueprints(app)
    setup_logger(app)

    @app.route('/')
    def index():
        # return 200 for zappa health check
        return ""

    return app

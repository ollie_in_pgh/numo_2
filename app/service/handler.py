import itertools

from fast_autocomplete import AutoComplete

from app.constants import WORD_FILE_PATH, SEARCH_RETURN_SIZE

"""
To add new search functionality (Redis, ElasticSearch, etc.)
create a new search class with a "search()" method.
For an example look at the _DawgSearcher class.
Then add that class to the Search.searcher_registry with 
a unique key.
- Ollie 2020-12-17
"""


class FileHandler:
    def __init__(self, file_path=None):
        if not file_path:
            file_path = WORD_FILE_PATH
        self.file_path = file_path
        self.words_dict = self._get_words_dict()

    def _get_words_dict(self):
        with open(self.file_path, "r") as f:
            words = {
                line.strip().lower(): {}
                for line in f.readlines()
            }

        return words

    def get_scrabble_corpus(self):
        return self.words_dict


class _TrieNode:
    def __init__(self, character):
        self.char = character
        self.children = {}
        self.is_end = False


class _TrieSearcher:
    def __init__(self, corpus):
        self.root = _TrieNode("")
        self.size = SEARCH_RETURN_SIZE
        if not corpus:
            handler = FileHandler()
            corpus = handler.get_scrabble_corpus()
        self._insert_all_corpus(corpus)

    def _insert_all_corpus(self, words):
        for word in words:
            self._insert(word)

    def _insert(self, word):
        current = self.root
        for char in word:
            if char not in current.children:
                current.children[char] = _TrieNode(char)

            current = current.children[char]

        current.is_end = True

    def _dfs_stack(self, node, prefix):
        output = []
        stack = [(node, prefix)]
        while stack:
            current, prefix = stack.pop()
            if current.is_end:
                output.append(prefix)
            if len(output) >= self.size:
                break
            for child in current.children.values():
                if child is None:
                    continue
                stack.append((child, prefix + child.char))

        return output

    def search(self, prefix):
        current = self.root
        for char in prefix:
            current = current.children.get(char)
            if not current:
                return []
        output = self._dfs_stack(current, prefix)
        return sorted(output)


class _DawgSearcher:
    def __init__(self, corpus=None):
        if not corpus:
            handler = FileHandler()
            corpus = handler.get_scrabble_corpus()
        self.autocomplete = AutoComplete(corpus)
        self.size = SEARCH_RETURN_SIZE

    def search(self, prefix):
        nested_list = self.autocomplete.search(prefix, size=self.size)
        chain = itertools.chain(*nested_list)
        return list(chain)


class Search:
    def __init__(self, searcher_name="dawg_searcher", corpus=None):
        self.searcher_registry = {
            "dawg_searcher": _DawgSearcher,
            "trie_searcher": _TrieSearcher
        }
        searcher = self.searcher_registry[searcher_name]
        self.searcher = searcher(corpus)

    def search(self, prefix):
        return self.searcher.search(prefix)

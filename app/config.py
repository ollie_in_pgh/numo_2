import os


class Config:
    SECRET_KEY = os.environ.get("NUMO_2_SECRET_KEY")
    CORS_ENABLED = True
    AWS_REGION = "us-east-2"
    NUMO_2_S3_BUCKET = None
    AUTOCOMPLETE_PREFIX = "autocomplete"
    SCRABBLE_FILE = "scrabble.txt"
    TEMP_DIRECTORY = "/tmp/"
    DEBUG = False
    TESTING = False
    LOG_TO_FILE = True
    LOG_TO_STD_OUT = False
    LOGMAXBYTES = 10240
    LOGBACKUPCOUNT = 10


class DevConfig(Config):
    MODE = "development"
    DEBUG = True
    NUMO_2_S3_BUCKET = "numo-2-dev"
    LOG_TO_FILE = False
    LOG_TO_STD_OUT = True


class TestConfig(Config):
    MODE = "testing"
    TESTING = True
    NUMO_2_S3_BUCKET = "numo-2-test"


class ProdConfig(Config):
    MODE = "production"
    TEMP_DIRECTORY = "/mnt/efs/"
    NUMO_2_S3_BUCKET = "numo-2-prod"


CONFIG_MAP = {
    'development': DevConfig,
    'testing': TestConfig,
    'production': ProdConfig
}

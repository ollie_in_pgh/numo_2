import logging

from flask import jsonify, abort

from app import searcher
from app.autocomplete import autocomplete_bp

logger = logging.getLogger(__name__)


@autocomplete_bp.route('/<prefix>', methods=['GET'])
def from_prefix(prefix):
    """
    ---
    GET:
      summary: Get single word autocomplete by prefix
      parameters:
        - in: path
          name: prefix
          description: To search for
          required: true
          schema:
            type: string
      responses:
        200:
          description:
            Up to 10 full words related to prefix
        400:
          description:
            There was an issue processing/validating the request body
        418:
          description:
            Could not find full words related to prefix
        500:
          description:
            Unknown error
    """
    result = searcher.search(prefix)

    if not result:
        msg = f"Could not find full words related to: {prefix}"
        logger.info(msg)
        abort(418, msg)

    return jsonify(result)

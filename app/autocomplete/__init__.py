from flask import Blueprint

autocomplete_bp = Blueprint("autocomplete", __name__)
from app.autocomplete import routes
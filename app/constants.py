import pathlib

CUR_DIR = pathlib.Path(__file__).parent.absolute()
SERVICE_DIR = pathlib.Path.joinpath(CUR_DIR, "service")
FILES_DIR = pathlib.Path.joinpath(CUR_DIR, "files")
WORD_FILE_NAME = "scrabble.txt"
WORD_FILE_PATH = pathlib.Path.joinpath(FILES_DIR, WORD_FILE_NAME)
SEARCH_RETURN_SIZE = 10

#!/usr/bin/env bash

if [[ ! -d /etc ]]
then
  echo "Downloading Levenshtein wheel."
  lev_url="https://files.pythonhosted.org/packages/25/cd/c0089792d7681738ac3663b4069f91b41bc0a81a915fe4e0038cc09952f8/python_Levenshtein_wheels-0.13.1-cp37-cp37m-manylinux1_x86_64.whl"
  curl -LO ${lev_url}
  wheel unpack python_Levenshtein_wheels-0.13.1-cp37-cp37m-manylinux1_x86_64.whl
  mv python_Levenshtein_wheels-0.13.1/Levenshtein ./Levenshtein
  rm python_Levenshtein_wheels-0.13.1-cp37-cp37m-manylinux1_x86_64.whl
  rm -rf python_Levenshtein_wheels-0.13.1
fi

zappa update dev
echo "Deployed dev. Check https://8fjeg6lhu6.execute-api.us-east-2.amazonaws.com/dev/autocomplete/blah"